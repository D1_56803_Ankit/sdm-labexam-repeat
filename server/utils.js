function createResult(error,data){
    const result = {};
    if(error){
        result["status"]="error";
        result["error"]=error;
    }
    else{
        result["status"]="success";
        result["data"]=data;
    }
    return result;
}

const mysql = require("mysql2")

const openConnection=()=>{
    const connection = mysql.createConnection({
        host: "db",
        user: "root",
        password: "manager",
        database: "mydb"
    });
    connection.connect();
    return connection;
};
module.exports={
    createResult,
    openConnection,
};