const express = require('express');
const app=express();

const cors= require("cors");

const routerMovie=require("./routes/movies");

app.use(cors("*"));
app.use(express.json());
app.use("/movie", routerMovie);

app.listen(4007, "0.0.0.0", () => {
    console.log("server started on port 4007");
});