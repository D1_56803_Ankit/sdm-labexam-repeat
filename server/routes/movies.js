const { query } = require("express");
const express = require("express");
const router = express.Router();
const utils = require("../utils");

// display all data of movies
router.get("/all", (request, response) => {
    const connection = utils.openConnection();

  const statement = `
        select * from Movie 
      `;
  connection.query(statement, (error, result) => {
      response.send(utils.createResult(error, result));
    
  });
});

// display all data of movie
router.get("/:title", (request, response) => {
  const { title } = request.params;

  const connection = utils.openConnection();

  const statement = `
        select * from Movie where
        movie_title = '${title}'
      `;
  connection.query(statement, (error, result) => {

      response.send(utils.createResult(error, result));
    
  });
});

router.post("/add", (request, response) => {
  const { movie_title, movie_release_date, movie_time, director_name } = request.body;

  const connection = utils.openConnection();
  console.log(connection);
  const statement = `
        insert into Movie
          (movie_title,movie_release_date,movie_time,director_name)
        values
          ( '${movie_title}', '${movie_release_date}', '${movie_time}', '${director_name}')
      `;
  connection.query(statement, (error, result) => {

    response.send(utils.createResult(error, result));
  });
});

router.put("/update/:title", (request, response) => {
  const { title } = request.params;
  const { movie_release_date, movie_time } = request.body;

  const statement = `
    update Movie
    set
    movie_release_date="${movie_release_date}",
    movie_time="${movie_time}"
    where
    movie_title = '${title}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
    
        console.log(statement);

    response.send(utils.createResult(error, result));
  });
});

//delete a movie
router.delete("/remove/:title", (request, response) => {
  const { title } = request.params;
  const statement = `
    delete from Movie
    where
    movie_title = '${title}'
  `;
  const connection = utils.openConnection();
  connection.query(statement, (error, result) => {
   
    console.log(statement);
    response.send(utils.createResult(error, result));
  });
});

module.exports = router;
