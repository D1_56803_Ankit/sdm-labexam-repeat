
create table Movie
(movie_id INTEGER primary key auto_increment,
 movie_title VARCHAR(20), 
movie_release_date DATE,
 movie_time TIME,
director_name VARCHAR(30));

INSERT into Movie(movie_title,movie_release_date,movie_time,director_name) VALUES ("Pushpaa", "2021-12-12", "12:00:00", "Rajamauli");
INSERT into Movie(movie_title,movie_release_date,movie_time,director_name) VALUES ("RRR", "2022-09-09", "11:00:00", "Aj prod");
INSERT into Movie(movie_title,movie_release_date,movie_time,director_name) VALUES ("Baraat", "2022-04-04", "12:00:00", "meghna prod");